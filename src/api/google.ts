export function recognise(paths: number[][][], width: number = 300, height : number = 300) : Promise<any> {
    const body = {
      itc: 'ja-t-i0-handwrit',
      app_version: 0.4,
      api_level: '537.36',
      input_type: '0',
      options: 'enable_pre_space',
      requests: [
        {
          writing_guide: {
            writing_area_width: width,
            writing_area_height: height,
          },
          pre_context: '',
          max_num_results: 10,
          max_completions: 0,
          language: 'ja',
          ink: paths,
        },
      ],
    };
  
    return fetch(
      'https://inputtools.google.com/request?itc=ja-t-i0-handwrit&app=translate',
      {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json;',
        },
        body: JSON.stringify(body),
      }
    ).then(resp => resp.json());
  }
  