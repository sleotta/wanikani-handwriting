import * as React from 'react';
import { Helmet } from 'react-helmet';
import { HashRouter, Route } from 'react-router-dom';

import { Canvas } from 'src/app/canvas';

interface IState {
	initDone : boolean,
}

class App extends React.Component<{}, IState> {
	public state = {
		initDone: false,
	};

	public render() {
		return (
			<div className="App container">
				<Helmet>
					<meta charSet="utf-8" />
					<title>Wanikani Handwriting</title>
				</Helmet>
				<HashRouter>
					<Route exact={true} path="/" component={Canvas} />
				</HashRouter>
			</div>
		);
	}
}

export default App;
