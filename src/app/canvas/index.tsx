import { debounce } from 'lodash';
import * as React from 'react';
import SvgSketchCanvas from 'react-sketch-canvas';
import { recognise } from 'src/api/google';

const styles = {
  border: '0.0625rem solid #9c9c9c',
  borderRadius: '0.25rem',
};

interface IState {
  response?: string[],
}

export class Canvas extends React.Component<{}, IState> {
  private canvas: any;

  constructor(props: {}) {
    super(props);
    this.state = {
      response: undefined,
    };
    this.canvas = null;

    this.printIt = debounce(this.printIt, 1000);
  }

  public componentDidMount() {
    const temp = this.canvas.handlePointerUp;
    this.canvas.handlePointerUp = (e: React.SyntheticEvent<HTMLElement>) => {
      temp(e);
      this.printIt();
    };
  }

  public printIt = async () => {
    const paths = await this.canvas.exportPaths();
    const pointArray: number[][][] = [];

    paths.forEach((path : Map<string, Array<Map<string, number>>>) => {
      const xArray: number[] = [];
      const yArray: number[] = [];
      (path.get('paths') || []).forEach((pair: Map<string, number>) => {
        xArray.push(pair.get('x') || 0);
        yArray.push(pair.get('y') || 0);
      });
      pointArray.push([xArray, yArray]);
    });
    const response = await recognise(pointArray);

    this.setState({
      response: response[1][0][1],
    });
  };

  public clear = () => {
    this.canvas.clearCanvas();
    this.setState({
      response: undefined,
    });
  };

  public render() {
    const { response } = this.state;

    const possibleAnswers =
      response &&
      response.filter((r: string) => r.length === 1).map((r: string) => (
        <div className="btn mr-2" key={r}>
          {r}
        </div>
      ));

    return (
      <React.Fragment>
        <SvgSketchCanvas
          ref={(element: any) => {
            this.canvas = element;
          }}
          width="300px"
          height="300px"
          styles={styles}
          strokeWidth={3}
          strokeColor="black"
        />
        <button
          type="button"
          className="btn btn-primary"
          onClick={this.printIt}
        >
          Log
        </button>
        <button type="button" className="btn" onClick={this.clear}>
          Clear
        </button>
        {possibleAnswers && <div>{possibleAnswers}</div>}
      </React.Fragment>
    );
  }
}
